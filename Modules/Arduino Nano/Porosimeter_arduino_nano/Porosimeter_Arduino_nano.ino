// include the library code:
#include <SoftwareSerial.h>

const byte RE = 11;
const byte DE = 10;

SoftwareSerial rs485 (12, 9);

String GP_MSG = "";
String PC_MSG = "";
float GP_Offset;
float GP_Current_PSI;
bool Zeroing = false;
int x = 0;
float GP_Zero_Average = 0;
int posSym = -1;

void setup() {
  Serial.begin(38400);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB
  }
  
  rs485.begin (19200);
  pinMode (RE, OUTPUT);
  pinMode (DE, OUTPUT);
  
}

void loop() {
  
  // Read from GP50
  digitalWrite (RE, HIGH); //Set to transmit
  digitalWrite (DE, HIGH);
  rs485.print("!003:SYS?\r");
  delayMicroseconds(100);
  digitalWrite (RE, LOW); //Set to receive
  digitalWrite (DE, LOW);

  for (byte i = 0; i < 200; i++) {
    delay(10);
    if (rs485.available() > 0) {
      GP_MSG = rs485.readStringUntil(13);
      posSym=GP_MSG.indexOf('+');
      if (posSym >= 0) {
        GP_MSG = GP_MSG.substring(posSym+1);
      }
      // Got a response, exit loop
      break;
    }
    // Failed to get a response, show -999.99
    GP_MSG = "-999.99";
  }

  delay(10);

  if (Zeroing==false) {
    // Write to PAXI Unit
    digitalWrite (RE, HIGH); //Set to transmit
    digitalWrite (DE, HIGH);
    rs485.println("N1VC" + GP_MSG + "$");
    delayMicroseconds(100);
    digitalWrite (RE, LOW); //Set to receive
    digitalWrite (DE, LOW);
  }


  if (Zeroing) {
    // Seems to be some delay after chaning the zero before getting a good reading again
    // Ignore the first few points that come in after zero
    x++;
    if (x > 20) {
      GP_Zero_Average = GP_Zero_Average + GP_MSG.toFloat();
    }
    if (x >=30) {
      GP_Offset = GP_Zero_Average/10;
      digitalWrite (RE, HIGH); //Set to transmit
      digitalWrite (DE, HIGH);
      rs485.print("!003:SZ=" + String(GP_Offset) + "\r");
      delayMicroseconds(100);
      digitalWrite (RE, LOW); //Set to receive
      digitalWrite (DE, LOW);
      Zeroing = false;
    }
  }

  delay(10);

  if (Zeroing==false) {
    if (Serial) {
      Serial.println("P:" + GP_MSG);  //Write to TX/RX lines
      if (Serial.available() > 0) {
        PC_MSG = Serial.readStringUntil(13);
        if (PC_MSG == "Zero") {
          // Zero routine
		      // Set current offset to zero to get raw readings
          digitalWrite (RE, HIGH); //Set to transmit
          digitalWrite (DE, HIGH);
          rs485.print("!003:SZ=0\r");
          delayMicroseconds(100);
          digitalWrite (RE, LOW); //Set to receive
          digitalWrite (DE, LOW);
          Zeroing = true;
          x = 0;
          GP_Zero_Average = 0;
        }
      }
    }
  }
delay(100);
}
